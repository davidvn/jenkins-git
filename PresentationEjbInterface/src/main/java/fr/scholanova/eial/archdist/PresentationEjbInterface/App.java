package fr.scholanova.eial.archdist.PresentationEjbInterface;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        /*PresentationEjbInterface : interface contenant les spéc
PresentationEjb : implémentation métier de l'interface qui sera Remote dans un EJB de type Singleton
PresentationEjbEAR : encapsulation des 2 projets précédents  dans un fichier .ear pour le déploiement dans JBoss
et PresentationEjbClient : application standalone Java (main), appelant les méthodes distantes de l'EJB déployé dans jboss démarré.*/
    }
}
